(function() {
  'use strict'

  const d = document,
      projects = d.querySelector('.projects'),
      articles = d.querySelector('.articles'),
      profile = d.querySelector('.profile')


  let profileView = false
  function showProfile() {
    let tl1 = new TimelineMax()
    if (!profileView) {
      profileView = true
      TweenMax.set([projects, articles], {y: -20})
      tl1
        .to([projects, articles], 0.8, {
          opacity: 0,
          y: 20,
          onComplete: function() {
            TweenMax.set([projects, articles], {display: 'none'})
            TweenMax.set(profile, {y: -20})
            TweenMax.to(profile, 0.8, {
              y: 20,
              display: 'block',
              opacity: 1,
            })
          }
      })
    } else {
      profileView = false
      tl1
        .to(profile, 0.8, {
          y: -20,
          opacity: 0,
          display: 'none',
          onComplete: function() {
            TweenMax.to([projects, articles], 0.8,{
              y: -20,
              opacity: 1,
              display: 'block'
            })
          }
        })
    }
  }

  let clickProfile = d.querySelector('.profile_btn'),
      closeProfile = d.querySelector('.close_profile'),
      activeProject = d.querySelector('.active_project'),
      tl2 = new TimelineMax(),
      projHeading = d.querySelector('.project_heading'),
      projDesc = d.querySelector('.project_description'),
      projImg = d.querySelector('.project_imgs'),
      projectView = false,
      cached

    clickProfile.addEventListener('click', showProfile, false)
    closeProfile.addEventListener('click', showProfile, false)

  const Projects = {

    p7: {
      title: 'Rugby 7s',
      description: 'Front-end development of main Rugby 7s website and development of shazam' +
      ' activation game. The game allowed users to flick the ball towards the goal posts. If the' +
      ' use hit one of three icons, they would win one of three prizes. Game was built using GSAP ' +
      'animation library.',
      img: [
        'img/runwild-desktop1.jpg',
        'img/runwild-desktop2.jpg',
        'img/runwild-mobile.png',
        'img/runwild-mobile-game1.png',
        'img/runwild-mobile-game2.png',
        'img/runwild-mobile-game3.png',
        'img/runwild-mobile-game4.png',
        'img/runwild-mobile-game5.png',
      ]
    },
    p6: {
      title: 'Alfa game',
      description: 'Game built with Phaser Javascript library for a shazam activation. Users' +
      ' need to match selections to win a prize.',
      img: [
        'img/alfa1-mobile1.jpg',
        'img/alfa1-mobile2.jpg',
        'img/alfa1-mobile3.jpg'
      ]
    },
    p5: {
      title: 'Landing pages & rich media creatives',
      description: 'Landing pages and rich media interstitials built with MRAID 2.0.',
      img: [
        'img/dv-lp-mobile1.jpg',
        'img/dv-lp-mobile2.jpg',
        'img/dv-lp-mobile3.jpg',
        'img/dv-rich-mobile1.jpg',
        'img/dv-rich-mobile2.jpg',
        'img/dv-rich-mobile3.jpg',
        'img/dv-rich-mobile4.jpg',
        'img/dv-rich-mobile5.jpg',
        'img/dv-rich-mobile6.jpg',
        'img/dv-rich-mobile7.jpg',
        'img/dv-rich-mobile8.jpg'
      ]
    },
    p4: {
      title: 'Digital Venture & in-house tools',
      description: 'Development of main Digital Venture Website and in-house tools, used by both ' +
      'the production team and sales to track campaign based tasks and journeys.',
      img: [
        'img/dv-desktop1.jpg',
        'img/dv-desktop2.jpg',
        'img/dv-desktop3.jpg',
        'img/dv-mobile1.png',
        'img/dv-mobile2.png',
        'img/dv-mobile3.png',
        'img/dv-tool1.jpg',
        'img/dv-tool2.jpg',
        'img/dv-tool3.jpg',
        'img/dv-tool4.jpg',
        'img/dv-tool5.jpg',
        'img/dv-tool6.jpg'
      ]
    },
    p3: {
      title: 'Get Ready Napier',
      description: 'Employed as an creative intern at Napier my key responsibilities were overseeing the redesign and development of the official Get Ready student portal. From working closely with facility staff and planning the initial brief and design to full development of site features such as animated info-grahics, user testing, responsive' +
      ' design and ﬁnal deployment. \n' +
      '\n',
      img: [
        'img/getready-desktop5.jpg',
        'img/getready-desktop3.jpg',
        'img/getready-desktop2.jpg',
        'img/getready1-mobile.png',
        'img/getready2-mobile.png'
      ]
    },
    p2: {
      title: 'scpianolessons.ie',
      description: 'Website designed/developed for local piano teacher.',
      img: [
        'img/scpianolessons-desktop1.jpg',
        'img/scpianolessons-desktop2.jpg',
        'img/scpianolessons-desktop3.jpg',
        'img/scpianolessons-mobile1.png',
        'img/scpianolessons-mobile2.png',
        'img/scpianolessons-mobile3.png'
      ]
    },
    p1: {
      title: 'clarkesbar.ie',
      description: 'Website designed/developed for Irish bar.',
      img: [
        'img/clarkes-desktop1.jpg',
        'img/clarkes-mobile1.png',
        'img/clarkes-mobile2.png',
      ]
    },
    showProject: function(i) {
      //console.log(this['p' + i].title)
      projHeading.innerText = this['p' + i].title
      projDesc.innerText = this['p' + i].description
      let projectImg = this['p' + i].img,
          mobStr = 'mobile',
          deskStr = 'desktop'

      if (cached !== i) {
        projImg.innerHTML = '';

        for (let i = 0; i < projectImg.length; i++) {
          let img = d.createElement('img')
          img.src = projectImg[i]

          if (projectImg[i].indexOf(mobStr) !== -1) {
            img.className = 'mobile_img'
          } else {
            img.className = 'desktop_img'
          }
          projImg.appendChild(img);
          cached = i
        }
      }

      if (!profileView) {
        projectView = true
        TweenMax.set([projects, articles], {y: -20})
        tl2
          .to([projects, articles], 0.8, {
            opacity: 0,
            y: 20,
            onComplete: function() {
              TweenMax.set([projects, articles], {display: 'none'})
              TweenMax.set(activeProject, {y: -20})
              TweenMax.to(activeProject, 0.8, {
                y: 20,
                display: 'block',
                opacity: 1,
              })
            }
          })
      }
    },
    closeProject: function() {
      projectView = false
      tl2
        .to(activeProject, 0.8, {
          y: -25,
          opacity: 0,
          display: 'none',
          onComplete: function() {
            TweenMax.to([projects, articles], 0.8,{
              y: -25,
              opacity: 1,
              display: 'block'
            })
          }
        })
    }
  }

  let p = Object.create(Projects),
      clickedProject = d.getElementsByClassName('project'),
      closeProject = d.querySelector('.close_project')

  closeProject.addEventListener('click', function(e) {
    p.closeProject()
  })

  for (let i = 0; i < clickedProject.length; i++) {
    clickedProject[i].addEventListener('click', function(e) {
      switch(this.getAttribute('data-id')) {
        case '1':
          p.showProject('7')
          break;
        case '2':
          p.showProject('6')
          break;
        case '3':
          p.showProject('5')
          break;
        case '4':
          p.showProject('4')
          break;
        case '5':
          p.showProject('3')
          break;
        case '6':
          p.showProject('2')
          break;
        case '7':
          p.showProject('1')
          break;
      }
    })
  }

  let clickedArticle = d.getElementsByClassName('article')
  for (let i = 0; i < clickedArticle.length; i++) {
    clickedArticle[i].addEventListener('click', function(e) {
      switch(this.getAttribute('data-id')) {
        case 'a1':
          window.open('https://johncash.in/staging/pokedex/index.html', '_blank');
          break;
      }
    })
  }

})()