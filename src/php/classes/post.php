<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 20/09/2017
 * Time: 10:20
 */

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    require_once("../libs/phpmailer/PHPMailerAutoload.php");

    $name   = strip_tags(trim($_POST["name"]));
    $name   = str_replace(array("\r", "\n"), array(" ", " "), $name);
    $phone  = strip_tags(trim($_POST["tel"]));
    $phone  = str_replace(array("\r", "\n"), array(" ", " "), $phone);
    $email  = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
    $message  = strip_tags(trim($_POST["message"]));
    $message  = str_replace(array("\r", "\n"), array(" ", " "), $message);


    //phpmailer
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Host = 'smtp.postmarkapp.com';
    $mail->SMTPAuth = true;
    $mail->Username = '72670caa-9ef5-4a60-b1eb-a630b81fa038';
    $mail->Password = '72670caa-9ef5-4a60-b1eb-a630b81fa038';
    $mail->SMTPSecure = 'TLS';
    $mail->Port = 587;
    $mail->CharSet = 'UTF-8';
    $mail->setFrom('campaigns@digitalventure1.com', 'Sarah Cashin');
    $mail->addReplyTo('campaigns@digitalventure1.com', 'Sarah Cashin');

    $mail->addAddress('sarahcashinpianolessons@gmail.com', 'Sarah');

    $emailOutput = file_get_contents('email.html');
    $emailOutput = str_replace('%name%', $name, $emailOutput);
    $emailOutput = str_replace('%phone%', $phone, $emailOutput);
    $emailOutput = str_replace('%email%', $email, $emailOutput);
    $emailOutput = str_replace('%message%', $message, $emailOutput);

    $mail->Subject = 'scpianolessons.ie message';
    $mail->isHTML(true);
    $mail->MsgHTML($emailOutput);
    $mail->AltBody = '';

    if (!$mail->send()) {
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        //close connection
    }

} else {
    // Not a POST request, set a 403 (forbidden) response code.
    http_response_code(403);
    echo "There was a problem with your submission, please try again.";
}